﻿import Arzmodule

print('Основний масив: ')
a=[4,16,7,20,11,3,1,0,8,10,24,10]
print(a)
print('Швидке сортування: ')
print(Arzmodule.quick_sort.__doc__)
print(Arzmodule.quick_sort(a))
search=input("Введіть число: ")
search=int(search)
print(Arzmodule.element_znach.__doc__)
print(Arzmodule.element_znach(search, a))
print('Введіть послідовність чисел: ')
search2 = list(map(int, input('').split()))
print(Arzmodule.poslid_search.__doc__)
answer = (Arzmodule.poslid_search(a, search2))
if answer==True:
    print('Послідовність є')
else:
    print('Послідовності немає')

print('\n')
print('5 мінімальних елементів: ')
print(Arzmodule.fivemin.__doc__)
print(Arzmodule.fivemin(a))
print('5 максимальних елементів: ') 
print(Arzmodule.fivemax.__doc__)
print(Arzmodule.fivemax(a))
print('Середнє арифметичне елементів: ') 
print(Arzmodule.average.__doc__)
print(Arzmodule.average(a))
print('\n')
print('Масив без повторень: ')
print(Arzmodule.norepeats.__doc__)
print(Arzmodule.norepeats(a))


