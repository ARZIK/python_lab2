﻿def quick_sort(a):
    """Рекурсивне сортування з вибором опорного елементу"""
    if len(a) <= 1:
        return a

    element = a[0]
    left = list(filter(lambda x: x<element, a))
    center = list(filter(lambda x: x==element, a))
    right = list(filter(lambda x: x>element, a))

    return quick_sort(left) + center + quick_sort(right)


def element_znach(search, a):
    """Функція, яка перевіряє чи є число у списку"""
    if search in a:
        return "Це число є в списку"
    else:
        return "Цього числа немає в списку"

def poslid_search(a, search2):
    """З заданого підсписку буде визначатись чи є послідовність чисел між головним та підсписком"""
    for i in range(len(a)-len(search2)):
        if a[i:i+len(search2)] == search2:
            return True
 
    return False

def fivemin(a):
    """Функція повертає п'ять мінімальних елементів списка"""
    return sorted(list(set(a)))[:5]
    
def fivemax(a):
    """Функція повертає п'ять максимальних елементів списка"""
    return sorted(list(set(a)))[-5:]

def average(a):
    """Функція повертає середнє арифметичне списка"""
    return (sum(a)/len(a))

def norepeats(a):
    """Функція, яка повертає унікальні числа"""
    listnorep = []
    norep = set(a)

    for a in norep:
        listnorep.append(a)

    return listnorep




